/*
 * Copyright (C) Tamas Szabo
 *
 * This file is part of paparazzi

 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef MAVPARROT_C
#define MAVPARROT_C


#include <stdlib.h>
//#include <arpa/inet.h>
#include <string.h>

#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
//#include <sys/types.h>

#include <unistd.h>
#include <stdio.h>
#include <time.h>
#if (defined __QNX__) | (defined __QNXNTO__)
  #include <unix.h>
#else
 #include <sys/time.h>
 #include <time.h>
 #include <arpa/inet.h>
#endif

// Include Paparazzi headers
#include "modules/mavParrot/mavParrot.h"

#include "generated/airframe.h"
#include "state.h"

#include "firmwares/rotorcraft/navigation.h"
#include "firmwares/rotorcraft/autopilot.h"

#include "subsystems/gps.h"
#include "subsystems/commands.h"
#include "subsystems/radio_control.h"
#include "subsystems/electrical.h"
#include "subsystems/datalink/downlink.h"

#include "subsystems/navigation/common_flight_plan.h"

#include "math/pprz_algebra_int.h"
#include "math/pprz_geodetic_float.h"

#define MAVSEND_BUFFER_LENGTH 2041
//#define RAD2DEG (180/3.1415926)

#ifndef max
	#define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
#endif

#ifdef MAVLINK_USE_TCP
  fd_set 		masterFD; // file descriptor list for datalinks
  struct timeval	mavlinkWaitForDataTimeout; // Time struct for timeout fo the waits for data.

  char 		mavParrotConnected;
  int32_t 	mavParrotDownlinkListener;
  int32_t 	mavParrotDownlinkSockID;
  uint64_t	mavParrotLastHBTime;
#else
  #include <fcntl.h>
  
  int sock;
  struct sockaddr_in gcAddr; 
#endif
  
int16_t mavlink_mission_transmit_count;
int16_t mavlink_mission_transmit_id;

#define NUM_FLIGHTPLAN_SKIP_BLOCKS 4

// Waypoint 0 of paparazzi is not a real waypoint! Skip it
#define NUM_WP_SKIP_BEGIN 1

// Initialize Downlink
void mavParrot_init()
{ 
  printf("Initializing...\n");
#ifdef MAVLINK_USE_TCP
  mavParrotConnected 	= 0;
  mavParrotLastHBTime 	= 0;
  
  // Initialize the WP transmit variables
  mavlink_mission_transmit_count = 0;
  mavlink_mission_transmit_id = -1;
  //BEGIN: Non-blocking bind to TCP port on all addresses
  int yes=1;        // for setsockopt() SO_REUSEADDR, below
  int rv;

  struct addrinfo hints, *ai, *p;

  FD_ZERO(&masterFD);
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if ((rv = getaddrinfo(NULL, MAVLINK_LISTEN_PORT, &hints, &ai)) != 0) {
      exit(1);
  }
  
  for(p = ai; p != NULL; p = p->ai_next) {
      mavParrotDownlinkListener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
      if (mavParrotDownlinkListener < 0) { 
	  continue;
      }
	      setsockopt(mavParrotDownlinkListener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
      if (bind(mavParrotDownlinkListener, p->ai_addr, p->ai_addrlen) < 0) {
	  close(mavParrotDownlinkListener);
	  continue;
      }
      break;
  }
  
  if (p == NULL) {
      printf("Failed to bind\n");
      exit(2);
  }

  freeaddrinfo(ai);

  printf("Listening...\n");
  if (listen(mavParrotDownlinkListener, 10) == -1) {
      printf("Can not listen on bound port.\n");
      exit(3);
  }
  FD_SET(mavParrotDownlinkListener, &masterFD);
  mavlinkWaitForDataTimeout.tv_sec = 0;
  mavlinkWaitForDataTimeout.tv_usec = 0;
  //END: Non-blocking bind
#else
  //BEGIN: UDP Connection
  sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  struct sockaddr_in locAddr;

  memset(&locAddr, 0, sizeof(locAddr));
  locAddr.sin_family 		= AF_INET;
  locAddr.sin_addr.s_addr 	= INADDR_ANY;
  locAddr.sin_port 		= htons(MAVLINK_LISTEN_PORT);
  if (-1 == bind(sock,(struct sockaddr *)&locAddr, sizeof(struct sockaddr)))
  {
	  printf("\n\tBind failed: %s\n", strerror(errno));
	  close(sock);
	  exit(EXIT_FAILURE);
  } 
  /* Attempt to make it non blocking */
  if (fcntl(sock, F_SETFL, O_NONBLOCK | FASYNC) < 0)
  {
	  close(sock);
	  exit(EXIT_FAILURE);
  }

  memset(&gcAddr, 0, sizeof(gcAddr));
  gcAddr.sin_family 		= AF_INET;
  gcAddr.sin_addr.s_addr 	= inet_addr(GS_IP_ADDRESS);
  gcAddr.sin_port 		= htons(14550);

  //END: UDP Connection
#endif
  printf("Init Done\n");
}
//Periodic functions
void mavParrot_periodic_heatbeat() 
{
  mavlink_message_t msg;

  // Get the base modes of the mav - TODO: uses MAV_MODES_FLAGS enum. Should it be MAV_MODES enum???
  uint8_t baseModes = 0;
  if ((autopilot_motors_on) && 
      (!kill_throttle) && 
      (autopilot_mode != AP_MODE_FAILSAFE) && 
      (autopilot_mode != AP_MODE_KILL) )
						{ baseModes |= MAV_MODE_FLAG_SAFETY_ARMED;}
//   if ((autopilot_rc) || 
//       (autopilot_mode == AP_MODE_RC_DIRECT) ) 	
						{ baseModes |= MAV_MODE_FLAG_MANUAL_INPUT_ENABLED; }
  if ((autopilot_mode == AP_MODE_ATTITUDE_Z_HOLD) ||
      (autopilot_mode == AP_MODE_ATTITUDE_CLIMB) ||
      (autopilot_mode == AP_MODE_HOVER_CLIMB) ||
      (autopilot_mode == AP_MODE_HOVER_DIRECT) ||
      (autopilot_mode == AP_MODE_ATTITUDE_DIRECT) )
						{ baseModes |= MAV_MODE_FLAG_STABILIZE_ENABLED; }
  if (autopilot_mode == AP_MODE_NAV)
						{ baseModes |= MAV_MODE_FLAG_GUIDED_ENABLED;}
    
// 4	MAV_MODE_FLAG_AUTO_ENABLED	<= TODO: Which paparazzi mode means full autonomous flight?
 


  mavlink_msg_heartbeat_pack(AC_ID, 0, &msg, 
			     MAV_TYPE_QUADROTOR,
			     MAV_AUTOPILOT_PPZ,
			     baseModes,
			     0, 
			     MAV_STATE_ACTIVE		// status!
			    );
  
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_status() 
{
  mavlink_message_t msg;
  
  /* Send Status */ 
  mavlink_msg_sys_status_pack(AC_ID, 0, &msg, 
			      ARDRONE_SENSORS_PRESENT, 	// On-board sensors: present 		(bitmap)
			      ARDRONE_SENSORS_ACTIVE, 	// On-board sensors: active		(bitmap)
			      ARDRONE_SENSORS_ACTIVE, 	// On-board sensors: state		(bitmap)
			      -1,//10*sys_mon.cpu_load,	// System loadof main-loop time 	(0=0% to 1000=100%)
			      100*electrical.vsupply,	// Battery voltage			(milivolts)
			      electrical.current/10,	// Battery current			(10x miliampere)
			      -1,			// Battery remaining			(0-100 in %)
			      0, 			// Communication packet drops 		(0=0% to 10000=100%)
			      0, 			// Communication error(per packet)	(0=0% to 10000=100%)
			      0, 			// Autopilot specific error 1
			      0, 			// Autopilot specific error 2
			      0, 			// Autopilot specific error 3
			      0				// Autopilot specific error 4
			     );
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_position() 
{
  
}
void mavParrot_periodic_attitude() 
{ 
  mavlink_message_t msg;
//   mavlink_msg_attitude_pack(AC_ID, MAV_COMP_ID_IMU, &msg, microsSinceEpoch(), 
// 			    ahrs_impl.eulers.phi, 			// Phi
// 			    ahrs_impl.eulers.theta, 			// Theta
// 			    ahrs_impl.eulers.psi, 			// Psi
// 			    stateGetBodyRates_f()->p,			// p
// 			    stateGetBodyRates_f()->q,			// q
// 			    stateGetBodyRates_f()->r			// r
// 			   );			
 mavlink_msg_attitude_pack(AC_ID, MAV_COMP_ID_IMU, &msg, microsSinceEpoch(), 
			    stateGetNedToBodyEulers_f()->phi, 		// Phi
			    stateGetNedToBodyEulers_f()->theta, 	// Theta
			    stateGetNedToBodyEulers_f()->psi, 		// Psi
			    stateGetBodyRates_f()->p,			// p
			    stateGetBodyRates_f()->q,			// q
			    stateGetBodyRates_f()->r);			// r
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_attitude_quaternion()
{
  mavlink_message_t msg;
  mavlink_msg_attitude_quaternion_pack(AC_ID, MAV_COMP_ID_IMU, &msg, microsSinceEpoch(), 
			    stateGetNedToBodyQuat_f()->qi, 		// q1
			    stateGetNedToBodyQuat_f()->qx, 		// q2
			    stateGetNedToBodyQuat_f()->qy, 		// q3
			    stateGetNedToBodyQuat_f()->qz, 		// q4
			    stateGetBodyRates_f()->p,			// p
			    stateGetBodyRates_f()->q,			// q
			    stateGetBodyRates_f()->r);			// r
  
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_global_position() 
{
  mavlink_message_t msg;   
 
  int32_t posLon = gps.lla_pos.lon;
  int32_t posLat = gps.lla_pos.lat;
  
  int16_t speedX = ((int16_t)(stateGetSpeedEnu_i()->x/INT32_SPEED_OF_CM_S));
  int16_t speedY = ((int16_t)(stateGetSpeedEnu_i()->y/INT32_SPEED_OF_CM_S));
  int16_t speedZ = ((int16_t)(stateGetSpeedEnu_i()->z/INT32_SPEED_OF_CM_S));
  mavlink_msg_global_position_int_pack(AC_ID, MAV_COMP_ID_GPS, &msg, microsSinceEpoch(), 
				       
				       INT32_DEG_OF_RAD(gps.lla_pos.lat),
				       INT32_DEG_OF_RAD(gps.lla_pos.lon),
				       gps.hmsl,		// Above MSL
				       state.alt_agl_f, 	// Above ground!
				       speedX, 
				       speedY, 
				       speedZ,
				       65535);
 
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_local_position()
{
  mavlink_message_t msg;
  
  mavlink_msg_local_position_ned_pack(AC_ID,MAV_COMP_ID_ALL, &msg, microsSinceEpoch(),
					stateGetPositionNed_f()->x,
					stateGetPositionNed_f()->y,
					stateGetPositionNed_f()->z,
				        stateGetSpeedNed_f()->x,
				        stateGetSpeedNed_f()->y,
				        stateGetSpeedNed_f()->z
  );
  
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_gps_state() 
{

}
void mavParrot_periodic_gps_raw_int()
{
  
}
void mavParrot_periodic_HUD()
{
  mavlink_message_t msg;
  
  int16_t heading = (int16_t)((180.0/M_PI)*atan2(stateGetSpeedNed_f()->y,stateGetSpeedNed_f()->x));
  if (heading < 0) { heading += 360; }
  mavlink_msg_vfr_hud_pack(AC_ID, MAV_COMP_ID_IMU, &msg,			       
			   (int32_t)(state.airspeed_f*10000000),
			   sqrt(pow(stateGetSpeedNed_f()->x,2) + pow(stateGetSpeedNed_f()->y,2)),
			   heading,									// Heading
			   (commands[COMMAND_THRUST]*100)/MAX_PPRZ,					// Throttle
			   (float)(gps.hmsl/1000), 							// Altitude
			   stateGetSpeedEnu_f()->z							// Climb rate
			    );

  //printf("Heading: %d\n",heading);
  sendMavlinkMsgToGS(&msg);
}

//Check if data is available, if there is, handle it
void mavParrot_event() 
{
#ifdef MAVLINK_USE_TCP
  fd_set read_fds = masterFD;
  
  if (select(max(mavParrotDownlinkListener,mavParrotDownlinkSockID)+1, &read_fds, NULL, NULL, &mavlinkWaitForDataTimeout) == -1) {
	printf("Select failed!\n");
	return;
  }
  if (mavParrotConnected == 0)
  {
    if (FD_ISSET(mavParrotDownlinkListener, &read_fds))
    {
      printf("We have connection\n");
      struct sockaddr_storage remoteaddr;
      socklen_t addrlen = sizeof(remoteaddr);
      mavParrotDownlinkSockID = accept(mavParrotDownlinkListener,
	  (struct sockaddr *)&remoteaddr,
	  &addrlen);
      if (mavParrotDownlinkSockID == -1)
      {
	printf("Could not accept connection\n");
	return;
      }
      FD_SET(mavParrotDownlinkSockID, &masterFD);
      
      mavParrotConnected = 1;
      mavParrotLastHBTime = microsSinceEpoch();
      printf("Connection Accepted.\n");
    }
  }
  else
  {
    /*if ((microsSinceEpoch() - mavParrotLastHBTime) > MAVLINK_CONNECTION_TIMEOUT)
    {
      close(mavParrotDownlinkSockID); // Close socket
      
      FD_CLR(mavParrotDownlinkSockID, &masterFD); // Remove from watchlist
      mavParrotConnected 	= 0;	// We're unconnected
      mavParrotDownlinkSockID 	= -1;	// Invalid connection
      
      return; // Nothing to do here anymore ...
    }*/
  }
  if(mavParrotDownlinkSockID < 0) { return; }
  if (!FD_ISSET(mavParrotDownlinkSockID, &read_fds)) { return; }
#endif
  char tmpBuff[16];
  uint8_t buf[MAVSEND_BUFFER_LENGTH];
  memset(buf, 0, MAVSEND_BUFFER_LENGTH);
  
  ssize_t recsize;
  int i = 0;	
  
  recsize = readMavlinkMsgFromGS((void *)buf, MAVSEND_BUFFER_LENGTH);
  if (recsize > 0)
  {
    mavlink_message_t msg;
    mavlink_status_t status;
#ifdef MAVLINK_USE_TCP
    mavParrotLastHBTime = microsSinceEpoch(); // Need this to check connection timeout
#endif
    for (i = 0; i < recsize; ++i)
    {
      if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status))
      {
	switch (msg.msgid)
	{
	case MAVLINK_MSG_ID_HEARTBEAT:
	  break;
	case MAVLINK_MSG_ID_MISSION_REQUEST_LIST:
	  {
	    int targetSystem 	= msg.sysid; //mavlink_msg_mission_request_list_get_target_system(&msg);
	    //int targetComponent = msg.compid; //mavlink_msg_mission_request_list_get_target_component(&msg);
	    
	    printf("Received Mission Request List from %d: Target: %d Component: %d\n", msg.sysid,mavlink_msg_mission_request_list_get_target_system(&msg), mavlink_msg_mission_request_list_get_target_component(&msg));

	      //Reply with Mission Count
	      mavlink_msg_mission_count_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER ,&msg, 
					     targetSystem, 
					     MAV_COMP_ID_MISSIONPLANNER,
					     nb_waypoint-NUM_WP_SKIP_BEGIN
					     );
	      sendMavlinkMsgToGS(&msg);
	  }
	  break;
	case MAVLINK_MSG_ID_MISSION_REQUEST:
	  {
		int targetSystem 	= msg.sysid; //mavlink_msg_mission_request_list_get_target_system(&msg);
		int targetComponent = msg.compid; //mavlink_msg_mission_request_list_get_target_component(&msg);
		int id = mavlink_msg_mission_request_get_seq(&msg) + NUM_WP_SKIP_BEGIN;
		
		
		uint8_t wpCmd = MAV_CMD_NAV_WAYPOINT; // Define the waypoint action here!
		uint8_t current = 0; 		// Mission item is the current item
		
		if (id == 0) {current = 1;}	// Make the first waypoint the active one! REPLACE THIS WITH A CHECK!
		
		uint8_t autocontinue = 1; 	// Autocontinue to next WP
		float radius = 0;		// ONLY FOR NAV: Radius of acceptance
		float stayTime = 0;		// ONLY FOR NAV: Time to stay within the radius [miliseconds]
		float orbitRadius = 2;		// ONLY FOR LOITER: Radius of loitering
		float yawAngle = 0;		// FOR NAV OR LOITER: heading direction (0=NORTH)
		
		
		// !!!NOTE: waypoints[id] is the relative value from the flight plan multiplied by 256!!!
		struct EnuCoor_i tmpenui;
		struct EcefCoor_i tmpecefi;
		struct LlaCoor_i tmpllai;
		
		int32_t wpX = waypoints[id].x/256;
		int32_t wpY = waypoints[id].y/256;
		int32_t wpZ = waypoints[id].z/256;
		tmpenui.x = wpX;
		tmpenui.y = wpY;
		tmpenui.z = wpZ;
		
		ecef_of_enu_point_i(&tmpecefi, &state.ned_origin_i, &tmpenui);
		lla_of_ecef_i(&tmpllai,&tmpecefi);
		
		float lat = (float)((double)(INT32_DEG_OF_RAD(tmpllai.lat))/10000000.0);
		float lon = (float)((double)(INT32_DEG_OF_RAD(tmpllai.lon))/10000000.0);
		
		// The altitude required is w.r.t the ground, Not the real Lla altitude!
		float alt = tmpenui.z;
	
		
		mavlink_msg_mission_item_pack(AC_ID, targetComponent, &msg,
						targetSystem, 
						targetComponent, 
						id, 
						MAV_FRAME_GLOBAL, 
						wpCmd,
						current, 
						autocontinue, 
						radius,
						stayTime, orbitRadius, yawAngle, 
						lat,
						lon,
						alt);

		sendMavlinkMsgToGS(&msg);
	  }
	  break;
	case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
	  //Requested parameter list for a component
	  printf("Received Parameter Request List from: %d: Target: %d Component: %d - Dummy!\n", msg.sysid,mavlink_msg_param_request_list_get_target_system(&msg), mavlink_msg_param_request_list_get_target_component(&msg));
	  
	  mavlink_msg_param_value_pack(AC_ID, MAV_COMP_ID_IMU, &msg,"test_param_1", 1.73, MAVLINK_TYPE_DOUBLE, 2, 1);
	  mavlink_msg_param_value_pack(AC_ID, MAV_COMP_ID_IMU, &msg,"test_param_2", 2.15, MAVLINK_TYPE_DOUBLE, 2, 2);
	  sendMavlinkMsgToGS(&msg);
	  
	  break;
	case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
	  mavlink_msg_param_request_read_get_param_id(&msg, tmpBuff);
	  printf("Received Parameter Read Request from %d.Target: %d Component: %d ID: %s Index: %d - NOT HANDLED!!!\n",msg.sysid, mavlink_msg_param_request_read_get_target_system(&msg), mavlink_msg_param_request_read_get_target_component(&msg), tmpBuff, mavlink_msg_param_request_read_get_param_index(&msg));
	  
	  break;
	case MAVLINK_MSG_ID_COMMAND_LONG:
	{
	  mavlink_command_long_t cmd;
	  mavlink_msg_command_long_decode(&msg,&cmd);
	  
	  printf("RECEIVED: Command: from %d.Target: %d Component: %d - Not Implemented!\n", msg.sysid, cmd.target_system, cmd.target_component);
	  
	  switch (cmd.command)
	  {
	    case MAV_CMD_NAV_TAKEOFF:
	      printf("\tTAKE-OFF. Pitch: %.2f Yaw: %.2f Lat: %.2f Long: %.2f Alt: %.2f\n",cmd.param1, cmd.param4, cmd.param5, cmd.param6, cmd.param7);
	      break;
	    default:
	      printf("\tUNKNOWN! ID:%d", cmd.command);
	  }
	}
	  break;
	case MAVLINK_MSG_ID_MISSION_ACK:
	  break;
	case MAVLINK_MSG_ID_RC_CHANNELS_OVERRIDE:
	{
	    mavlink_rc_channels_override_t cmd;
	    mavlink_msg_rc_channels_override_decode(&msg,&cmd);
	    
// 	    uint8_t throttle 	= MAVLINK_JOYSTICK_SET_VALUE_UNSIGNED(cmd.chan3_raw);
// 	    int8_t roll 	= MAVLINK_JOYSTICK_SET_VALUE_UNSIGNED(cmd.chan1_raw)-128;
// 	    int8_t pitch 	= 0 - ((int8_t)(MAVLINK_JOYSTICK_MODE/2 + 0.5)) + MAVLINK_JOYSTICK_SET_VALUE_UNSIGNED(cmd.chan2_raw);
// 	    int8_t yaw 		= MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan4_raw);
// 	    
// 	    parse_rc_4ch_datalink((uint8_t)MAVLINK_JOYSTICK_MODE, 
// 				  (uint8_t)MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan3_raw),
// 				  MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan1_raw),
// 				  ((int8_t)((-1)*MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan2_raw))),
// 				  MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan4_raw)
// 				 );
	    /*printf("Throttle: %d;\tRoll: %d;\tPitch: %d;\tYaw: %d; \n",
				  ((uint8_t)MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan3_raw)),
				  ((int8_t)MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan1_raw)),
				  ((int8_t)((-1)*MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan2_raw))),
				  ((int8_t)MAVLINK_JOYSTICK_SET_VALUE_SIGNED(cmd.chan4_raw)));*/
	    
	    
	    
	    mavlink_msg_rc_channels_raw_pack(cmd.target_system,cmd.target_component, &msg, microsSinceEpoch(), 
				      1, 
				      cmd.chan3_raw, 
				      cmd.chan1_raw,
				      cmd.chan2_raw, 
				      cmd.chan4_raw, 
				      65535, 
				      65535, 
				      65535, 
				      65535, 
				      255); // Nondetermined signal strength
	    sendMavlinkMsgToGS(&msg);
	}
	break;
	case MAVLINK_MSG_ID_REQUEST_DATA_STREAM:
	{
	    mavlink_request_data_stream_t cmd;
	    mavlink_msg_request_data_stream_decode(&msg,&cmd);	    
	    printf("RECEIVED: Datastream Request: %d/%d, Stream ID: %d, Start/Stop: %d Rate: %d\n",
	      cmd.target_system, cmd.target_component, cmd.req_stream_id, cmd.start_stop, cmd.req_message_rate);
	    
	    mavlink_msg_data_stream_pack(AC_ID, cmd.target_component, &msg,
						       cmd.req_stream_id, cmd.req_message_rate,0);
	    sendMavlinkMsgToGS(&msg);
	}
	  break;
	case MAVLINK_MSG_ID_SET_MODE:
	{
	  mavlink_set_mode_t cmd;
	  mavlink_msg_set_mode_decode(&msg,&cmd);
	  
	 // DroidPlanner specific implementation of mode switches!
	  printf("RECEIVED: SetMode Request: Target: %d BaseMode: %d CustomMode: %d\n", cmd.target_system, cmd.base_mode, cmd.custom_mode);
	  if (cmd.base_mode != 1)
	  {
	    printf("Unknown Base mode.\n");
	    return;
	  }
	  switch (cmd.custom_mode)
	  {
	    case 2: // Altitude hold mode! AUTO2
	      autopilot_set_mode(MODE_AUTO2);
	      printf("AUTO2 Set\n");
	    break;
	    case 9: // Land modes
	      printf("Land/Take-off Command:");
// 	      if () {} else {}
	      printf("\n");
	    break;
	    case 3: // Auto / NAV modes
	      autopilot_set_mode(AP_MODE_NAV);
	      printf("NAV mode set.\n");
	    break;
	    default:
	      printf("Unknown Custom Mode!\n");
	  }
	  // END: DroidPlanner specific implementation of mode switches!
	  
	}
	break;
	case MAVLINK_MSG_ID_MISSION_SET_CURRENT:
	{
	  mavlink_mission_set_current_t cmd;
	  mavlink_msg_mission_set_current_decode(&msg,&cmd);
	  
	  
// 	  uint16_t seq; ///< Sequence
// 	  uint8_t target_system; ///< System ID
// 	  uint8_t target_component; ///< Component ID
	  printf("MISSION_SET: Target:%d",cmd.seq);
	  NavGotoWaypoint(NUM_FLIGHTPLAN_SKIP_BLOCKS + cmd.seq);
	  
	}
	break;
	case MAVLINK_MSG_ID_MISSION_COUNT:
	{
	  mavlink_mission_count_t cmd;
	  mavlink_msg_mission_count_decode(&msg,&cmd);
	  
	  if ((uint8_t)cmd.count > (uint8_t)(nb_waypoint))
	  {
	    mavlink_msg_mission_ack_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, MAV_MISSION_NO_SPACE);
	    sendMavlinkMsgToGS(&msg);
	    printf("Mission Count Received: Count rejected: out of space. No. of items: %d while there is space for: %d\n", cmd.count, nb_waypoint);
 	    break;
	  }
	  mavlink_mission_transmit_count = cmd.count;
	  mavlink_mission_transmit_id = 0;
	  printf("Mission Count Received from:%d/%d: No. of Mission Items: %d\n",msg.sysid, msg.compid, mavlink_mission_transmit_count);
	  
	  mavlink_msg_mission_request_pack(AC_ID,MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, mavlink_mission_transmit_id);
	  sendMavlinkMsgToGS(&msg);
	}
	break;
	case MAVLINK_MSG_ID_MISSION_ITEM:
	{
	  mavlink_mission_item_t item;
	  mavlink_msg_mission_item_decode(&msg,&item);
// 	  printf("Mission item received: ID: %d, Frame: %d, CmdID: %d, Current: %d, Cont: %d, p1: %.3f, p2: %.3f, p3: %.3f, p4: %.3f, x: %.3f, y:%.3f, z:%.3f\n",
// 		  item.seq, item.frame, item.command, item.current, item.autocontinue, item.param1, item.param2, item.param3, item.param4, item.x, item.y, item.z);
	  
	  if (((uint16_t)item.seq) >= ((uint16_t)nb_waypoint))
	  {
	    mavlink_msg_mission_ack_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, MAV_MISSION_NO_SPACE);
	    sendMavlinkMsgToGS(&msg);
	    printf("Mission  rejected: out of space. Items no: %d while there is space for: %d\n", item.seq, nb_waypoint);
 	    break;
	  }
	  if (mavlink_mission_transmit_id != -1)
	  {
	    if ((item.seq != mavlink_mission_transmit_id) || (item.seq >= mavlink_mission_transmit_count))
	    {
	      mavlink_msg_mission_ack_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, MAV_MISSION_INVALID_SEQUENCE);
	      printf("Mission  rejected: invalid ID. Items no: %d while expecting: %d\n", item.seq, mavlink_mission_transmit_id);
	      sendMavlinkMsgToGS(&msg);
	      break;
	    }
	  }
	  
	  int16_t itemID;
	  itemID = item.seq + NUM_WP_SKIP_BEGIN; // Skip wp 0 - it wasn't sent either.
	  switch (item.frame)
	  {
	    case MAV_FRAME_GLOBAL:
	    {
	      // Get the Lla representation of the current WP - we need the altitude from it!
	      struct EnuCoor_i tmpenui;
	      struct EcefCoor_i tmpecefi;
	      struct LlaCoor_i tmpllai;
	      
	      int32_t wpX = waypoints[item.seq].x/256;
	      int32_t wpY = waypoints[item.seq].y/256;
	      int32_t wpZ = waypoints[item.seq].z/256;
	      tmpenui.x = wpX;
	      tmpenui.y = wpY;
	      tmpenui.z = wpZ;
	      
	      ecef_of_enu_point_i(&tmpecefi, &state.ned_origin_i, &tmpenui);
	      lla_of_ecef_i(&tmpllai,&tmpecefi);
		
		
	      /*Conversion taken from sw/airborne/firmware/rotorcraft/datalink.c*/
	      struct LlaCoor_i lla;
	      struct EnuCoor_i enu;
	      lla.lat = INT32_RAD_OF_DEG(item.x*10000000);
	      lla.lon = INT32_RAD_OF_DEG(item.y*10000000);
	      lla.alt = tmpllai.alt; 			// The waypoint altitude can not be changed for now.
	      
	      enu_of_lla_point_i(&enu,&ins_ltp_def,&lla);
	      //TODO: Fix this conversion:
	      enu.x = POS_BFP_OF_REAL(enu.x)/50;		// With dividing by 50, the results are reasonable - though still not accurate.
	      enu.y = POS_BFP_OF_REAL(enu.y)/50;
	      
	      enu.z = item.z*256;//waypoints[itemID].z;// POS_BFP_OF_REAL(enu.z)/100;	// Set the altitude of the WP to the same as it was before!
	      VECT3_ASSIGN(waypoints[itemID], enu.x, enu.y, enu.z);
	      DOWNLINK_SEND_WP_MOVED_ENU(DefaultChannel, DefaultDevice, &item.seq, &enu.x, &enu.y, &enu.z);
	    }
	    break;
	    default:
	    {
	      mavlink_msg_mission_ack_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, MAV_MISSION_UNSUPPORTED_FRAME);
	      printf("Mission item rejected: unsupported frame!\n");
	    }
	  }
	  if (++mavlink_mission_transmit_id < mavlink_mission_transmit_count)
	  {
	    mavlink_msg_mission_request_pack(AC_ID,MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, mavlink_mission_transmit_id);
	    printf("Requesting mission id: %d\n",mavlink_mission_transmit_id);
	    sendMavlinkMsgToGS(&msg);
	  }
	  else
	  {
	    mavlink_msg_mission_ack_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER, &msg, msg.sysid, msg.compid, MAV_MISSION_ACCEPTED);
	    printf("Mission item transmission: success\n");
	    sendMavlinkMsgToGS(&msg);
	  }
// target_system	uint8_t	System ID
// target_component	uint8_t	Component ID
// seq	uint16_t	Sequence
// frame	uint8_t	The coordinate system of the MISSION. see MAV_FRAME in mavlink_types.h
// command	uint16_t	The scheduled action for the MISSION. see MAV_CMD in common.xml MAVLink specs
// current	uint8_t	false:0, true:1
// autocontinue	uint8_t	autocontinue to next wp
// param1	float	PARAM1 / For NAV command MISSIONs: Radius in which the MISSION is accepted as reached, in meters
// param2	float	PARAM2 / For NAV command MISSIONs: Time that the MAV should stay inside the PARAM1 radius before advancing, in milliseconds
// param3	float	PARAM3 / For LOITER command MISSIONs: Orbit to circle around the MISSION, in meters. If positive the orbit direction should be clockwise, if negative the orbit direction should be counter-clockwise.
// param4	float	PARAM4 / For NAV and LOITER command MISSIONs: Yaw orientation in degrees, [0..360] 0 = NORTH
// x	float	PARAM5 / local: x position, global: latitude
// y	float	PARAM6 / y position: global: longitude
// z	float	PARAM7 / z position: global: altitude
	  
	}
	break;
	default:
	  // Packet received
	  printf("UNKNOWN: Received packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
	}
      }
    }
  }

// Check WP status
#ifdef MAVLINK_USE_TCP
  if (mavParrotConnected)
  {
#endif
//nav_block - Flight plan block
//   struct  EnuCoor_f *pos = stateGetPositionEnu_f();
//   if ()
#ifdef MAVLINK_USE_TCP
  }
#endif
}

/* QNX timer version */
#if (defined __QNX__) | (defined __QNXNTO__)
uint64_t microsSinceEpoch()
{
	
	struct timespec time;
	
	uint64_t micros = 0;
	
	clock_gettime(CLOCK_REALTIME, &time);  
	micros = (uint64_t)time.tv_sec * 100000 + time.tv_nsec/1000;
	
	return micros;
}
#else
uint64_t microsSinceEpoch()
{
	
	struct timeval tv;
	
	uint64_t micros = 0;
	
	gettimeofday(&tv, NULL);  
	micros =  ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
	
	return micros;
}
#endif


int32_t sendMavlinkMsgToGS(const mavlink_message_t *msg)
{
#ifdef MAVLINK_USE_TCP
  if ((mavParrotDownlinkSockID == -1) || (mavParrotConnected == 0))
  {
    return 0;
  }
  uint16_t len;
  uint8_t buf[MAVSEND_BUFFER_LENGTH];
  
  len = mavlink_msg_to_send_buffer(buf, msg);
  if (send(mavParrotDownlinkSockID, buf, len, 0) == -1) 
  {
      printf("Error while sending packet");
  }
  return 0;//sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
#else
  uint16_t len;
  uint8_t buf[MAVSEND_BUFFER_LENGTH];
  
  len = mavlink_msg_to_send_buffer(buf, msg);
  return sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
#endif
}
int32_t readMavlinkMsgFromGS(void *buffer, size_t msgSize)
{
#ifdef MAVLINK_USE_TCP
  int nbytes = 0;
  if ((nbytes = recv(mavParrotDownlinkSockID, buffer, msgSize, 0)) <= 0) { // Read error or closed connection
    if (nbytes == 0) {
	// Closed Connection
	printf("Connection to GS is lost. Waiting for new connections...");
    } else {
	printf("Could not receive message. Dropping connection to GS & waiting for a new one...");
    }
    close(mavParrotDownlinkSockID); 		// Close socket    
    FD_CLR(mavParrotDownlinkSockID, &masterFD); 	// Remove from watchlist
    mavParrotConnected 	= 0;		// We're unconnected
    mavParrotDownlinkSockID 	= -1;		// Invalid connection
  } 
  return nbytes;
#else
  socklen_t fromlen;
  return recvfrom(sock, buffer, msgSize, 0, (struct sockaddr *)&gcAddr, &fromlen);
#endif
}
#endif // ifndef MAVPARROT_TCP_C