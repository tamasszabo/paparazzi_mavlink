/*
 * Copyright (C) Tamas Szabo
 *
 * This file is part of paparazzi

 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */


// mavlink connection via TCP server
#ifndef MAVPARROT_TCP_H
#define MAVPARROT_TCP_H

#include "mavlink/include/v1.0/common/mavlink.h"
//#include "mavlink/include/v1.0/common/common.h"

//Other Includes
//#include <sys/types.h>

// For debugging!
//#define SIM_DEBUG

#ifdef SIM_DEBUG
  #include <stdio.h>
  #include <time.h>
#define DEBUG_FILE_LOCATION "/home/tamas/papMavLinkParrotDebug.txt"
#endif

// Mavlink protocol definitions
#define ARDRONE_SENSORS_PRESENT 3
#define ARDRONE_SENSORS_ACTIVE ARDRONE_SENSORS_PRESENT

// END: Mavlink protocol definitions


// Mavlink Joystick Definitions
#define JOYSTICK_UINT8_SCALE
//#define JOYSTICK_MAXPPZ_SCALE
#define MAVLINK_JOYSTICK_MODULATION_MIN 1000
#define MAVLINK_JOYSTICK_MODULATION_MAX 2000

#define MAVLINK_JOYSTICK_MODE 255.0
#define MAVLINK_JOYSTICK_SET_VALUE_UNSIGNED( _val) (									\
	      ((uint32_t)(_val - MAVLINK_JOYSTICK_MODULATION_MIN)*MAVLINK_JOYSTICK_MODE)/				\
	      (MAVLINK_JOYSTICK_MODULATION_MAX-MAVLINK_JOYSTICK_MODULATION_MIN))

#define MAVLINK_JOYSTICK_SET_VALUE_SIGNED( _val) (									\
	      ((int32_t)(_val - MAVLINK_JOYSTICK_MODULATION_MIN - 							\
	      ((int32_t)(MAVLINK_JOYSTICK_MODULATION_MAX - MAVLINK_JOYSTICK_MODULATION_MIN))/2)*MAVLINK_JOYSTICK_MODE)/		\
	      (MAVLINK_JOYSTICK_MODULATION_MAX-MAVLINK_JOYSTICK_MODULATION_MIN))

	      
	      
	      
#define MAVLINK_USE_TCP

#ifdef MAVLINK_USE_TCP
  #define MAVLINK_LISTEN_PORT "5760"
  #define MAVLINK_CONNECTION_TIMEOUT 15000000
#else
  #define MAVLINK_LISTEN_PORT 14549
  //#define GS_IP_ADDRESS "127.0.0.1" 		// - For QGroundControl!
  #define GS_IP_ADDRESS "192.168.209.17"		// - For the tablet
#endif


//Methods

extern uint64_t microsSinceEpoch();
extern int32_t sendMavlinkMsgToGS(const mavlink_message_t *msg);
extern int32_t readMavlinkMsgFromGS(void *buffer, size_t msgSize);


extern void ParrDLOG_clear();
extern void ParrDLOG(char* msg);
// END: For Debuggingg

//Module init
extern void mavParrot_init();

//Periodic function(s) - possibly for regular downlink: send & receive
extern void mavParrot_periodic_heatbeat();
extern void mavParrot_periodic_status();
extern void mavParrot_periodic_position();
extern void mavParrot_periodic_attitude();
extern void mavParrot_periodic_attitude_quaternion();
extern void mavParrot_periodic_global_position();
extern void mavParrot_periodic_local_position();
extern void mavParrot_periodic_gps_state();
extern void mavParrot_periodic_gps_raw_int();
extern void mavParrot_periodic_HUD();

//Event function for data received - check if data available on mavlink
extern void mavParrot_event();

#endif

