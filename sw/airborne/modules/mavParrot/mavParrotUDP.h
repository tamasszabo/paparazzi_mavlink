/*
 * Copyright (C) Tamas Szabo
 *
 * This file is part of paparazzi

 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef MAVPARROT_UDP_H
#define MAVPARROT_UDP_H

#include "mavlink/include/v1.0/common/mavlink.h"
#include "mavlink/include/v1.0/common/common.h"

// For debugging!
#define SIM_DEBUG

#ifdef SIM_DEBUG
  #include <stdio.h>
  #include <time.h>
#define DEBUG_FILE_LOCATION "/home/tamas/papMavLinkParrotDebug.txt"

#endif

//#define GS_IP_ADDRESS "127.0.0.1" // IP Address of GS
#define GS_IP_ADDRESS "192.168.209.16" // IP Address of GS
#define GS_IP_PORT_UDP 14551


// Mavlink protocol definitions
#define MAVLINK_UAV_MODES MAV_MODE_FLAG_STABILIZE_ENABLED|MAV_MODE_FLAG_GUIDED_ENABLED|MAV_MODE_FLAG_AUTO_ENABLED|MAV_MODE_FLAG_MANUAL_INPUT_ENABLED
// END: Mavlink protocol definitions

//Variables

uint8_t mavlinkGSSysID;
char mavlinkGCSInit;

//Methods

extern uint64_t microsSinceEpoch();
extern int sendMavlinkMsgToGS(const mavlink_message_t *msg);


extern void ParrDLOG_clear();
extern void ParrDLOG(char* msg);
// END: For Debuggingg

//Module init
extern void mavParrot_init();

//Periodic function(s) - possibly for regular downlink: send & receive
#include "mavParrotPeriodicMessages.h"

//Event function for data received - check if data available on mavlink
extern void mavParrot_event();

#endif

