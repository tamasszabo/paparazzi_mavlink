/*
 * Copyright (C) Tamas Szabo
 *
 * This file is part of paparazzi

 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

//From mavlink-ivy/interfaces/interface.h
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#if (defined __QNX__) | (defined __QNXNTO__)
/* QNX specific headers */
#include <unix.h>
#else
/* Linux / MacOS POSIX timer headers */
#include <sys/time.h>
#include <time.h>
#include <arpa/inet.h>
#endif
// END: from mavlink-ivy/interfaces

#include "modules/mavParrot/mavParrot.h"
#include "generated/airframe.h"
#include "state.h"
#include "subsystems/gps.h"
#include "firmwares/rotorcraft/navigation.h"

#include "math/pprz_algebra_int.h"

#define MAVSEND_BUFFER_LENGTH 2041 //Not sure why, but it was like this in the example from mavlink
#define RAD2DEG (180/3.1415926)

int sock;
struct sockaddr_in gcAddr; 


uint64_t microsSinceEpoch();

/* Initialize Downlink */
void mavParrot_init()
{
#ifdef SIM_DEBUG
  ParrDLOG_clear();
#endif
  ParrDLOG("Initializing...");

  sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  struct sockaddr_in locAddr;

  memset(&locAddr, 0, sizeof(locAddr));
  locAddr.sin_family 		= AF_INET;
  locAddr.sin_addr.s_addr 	= INADDR_ANY;
  locAddr.sin_port 		= htons(GS_IP_PORT_UDP);

  /* Bind the socket to port 14551 - necessary to receive packets from qgroundcontrol */ 
  if (-1 == bind(sock,(struct sockaddr *)&locAddr, sizeof(struct sockaddr)))
  {
#ifdef SIM_DEBUG
	  ParrDLOG("error bind failed");
#endif
	  close(sock);
	  exit(EXIT_FAILURE);
  } 

  /* Attempt to make it non blocking */
  if (fcntl(sock, F_SETFL, O_NONBLOCK | FASYNC) < 0)
  {
#ifdef SIM_DEBUG
	  ParrDLOG("error setting nonblocking");
#endif
	  close(sock);
	  exit(EXIT_FAILURE);
  }


  memset(&gcAddr, 0, sizeof(gcAddr));
  gcAddr.sin_family 		= AF_INET;
  gcAddr.sin_addr.s_addr 	= inet_addr(GS_IP_ADDRESS);
  gcAddr.sin_port 		= htons(14550);
#ifdef SIM_DEBUG
  ParrDLOG("Init: done");
#endif
  
  mavlinkGSSysID = 255;
  mavlinkGCSInit = 0;
}


//Periodic functions
void mavParrot_periodic_heatbeat() 
{
  mavlink_message_t msg;

  mavlink_msg_heartbeat_pack(AC_ID, 0, &msg, MAV_TYPE_QUADROTOR, MAV_AUTOPILOT_PPZ, MAVLINK_UAV_MODES, 0, MAV_STATE_ACTIVE);
  
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_status() 
{
  mavlink_message_t msg;
  
  /* Send Status */ 
  mavlink_msg_sys_status_pack(AC_ID, 0, &msg, 3, 3, 3, 500, 11000, -1, -1, 0, 0, 0, 0, 0, 0);
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_position() 
{
  
}
void mavParrot_periodic_attitude() 
{
  mavlink_message_t msg;
  mavlink_msg_attitude_pack(AC_ID, MAV_COMP_ID_IMU, &msg, microsSinceEpoch(), 
			    stateGetNedToBodyEulers_f()->phi, 		// Phi
			    stateGetNedToBodyEulers_f()->theta, 	// Theta
			    stateGetNedToBodyEulers_f()->psi, 		// Psi
			    stateGetBodyRates_f()->p,			// p
			    stateGetBodyRates_f()->q,			// q
			    stateGetBodyRates_f()->r);			// r
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_attitude_quaternion()
{
  mavlink_message_t msg;
  mavlink_msg_attitude_quaternion_pack(AC_ID, MAV_COMP_ID_IMU, &msg, microsSinceEpoch(), 
			    stateGetNedToBodyQuat_f()->qi, 		// q1
			    stateGetNedToBodyQuat_f()->qx, 		// q2
			    stateGetNedToBodyQuat_f()->qy, 		// q3
			    stateGetNedToBodyQuat_f()->qz, 		// q4
			    stateGetBodyRates_f()->p,			// p
			    stateGetBodyRates_f()->q,			// q
			    stateGetBodyRates_f()->r);			// r
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_global_position() 
{
  mavlink_message_t msg;
  
  mavlink_msg_global_position_int_pack(AC_ID, MAV_COMP_ID_GPS, &msg, microsSinceEpoch(), 
				       RAD2DEG*stateGetPositionLla_i()->lat, 
				       RAD2DEG*stateGetPositionLla_i()->lon, 
				       stateGetPositionLla_i()->alt, 
				       stateGetPositionLla_i()->alt, 
				       1000,		// Groundspeed: X 		stateGetSpeedEcef_i()->x
				       100, 		// Groundspeed: Y		stateGetSpeedEcef_i()->y
				       100,		// GroundSpeed: Z (UP!) 	stateGetSpeedEcef_i()->z
				       65535);
  sendMavlinkMsgToGS(&msg);
}
void mavParrot_periodic_gps_state() 
{

}
void mavParrot_periodic_gps_raw_int()
{
  
}
void mavParrot_periodic_HUD()
{
  mavlink_message_t msg;
  mavlink_msg_vfr_hud_pack(AC_ID, MAV_COMP_ID_IMU, &msg,			       
			   100,			// Airspeed 
			   98, 			// Groundspeed
			   180, 		// Heading
			   700, 	// Throttle
			   stateGetPositionLla_f()->alt, 		// Altitude
			   0.2);	// Climb rate
  
  sendMavlinkMsgToGS(&msg);
}

//Check if data is available, if there is, handle it
void mavParrot_event() 
{
  ssize_t recsize;
  socklen_t fromlen;
 
  int i = 0;	
  
  char tmpBuff[16];
  char *logBuff[500];
  uint8_t buf[MAVSEND_BUFFER_LENGTH];
  memset(buf, 0, MAVSEND_BUFFER_LENGTH);
  
  recsize = recvfrom(sock, (void *)buf, MAVSEND_BUFFER_LENGTH, 0, (struct sockaddr *)&gcAddr, &fromlen);
  if (recsize > 0)
  {
    mavlink_message_t msg;
    mavlink_status_t status;
    
    for (i = 0; i < recsize; ++i)
    {
      //unsigned int temp = buf[i];
      if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status))
      {
	switch (msg.msgid)
	{
	case MAVLINK_MSG_ID_HEARTBEAT:
	  {
	    if (mavlinkGCSInit == 0)
	    {
	      mavlinkGSSysID = msg.sysid;
	      mavlinkGCSInit = 1;
	      
	      mavlink_msg_mission_count_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER ,&msg, msg.sysid, MAV_COMP_ID_MISSIONPLANNER ,nb_waypoint);
	      sendMavlinkMsgToGS(&msg);
	      
	      mavlink_msg_param_value_pack(AC_ID, MAV_COMP_ID_IMU, &msg,"test_param_1", 1.73, MAVLINK_TYPE_DOUBLE, 1, 0);
	      sendMavlinkMsgToGS(&msg);
	      mavlink_msg_param_value_pack(AC_ID, MAV_COMP_ID_IMU, &msg,"test_param_2", 2.15, MAVLINK_TYPE_DOUBLE, 1, 1);
	      sendMavlinkMsgToGS(&msg);
	      ParrDLOG("GS Init.");
	    }
#ifdef SIM_DEBUG
	    mavlink_heartbeat_t hb;
 
	    mavlink_msg_heartbeat_decode(&msg, &hb);
	    sprintf(logBuff,"Received HEARTBEAT from %d type: %d; mode %d; status: %d; AP: %d", msg.sysid, hb.type, hb.base_mode, hb.system_status, hb.autopilot);
	    ParrDLOG(logBuff);
#endif
	  }
	  break;
	case MAVLINK_MSG_ID_MISSION_REQUEST_LIST:
	  {
	    int targetSystem 	= msg.sysid; //mavlink_msg_mission_request_list_get_target_system(&msg);
	    int targetComponent = msg.compid; //mavlink_msg_mission_request_list_get_target_component(&msg);
	    
	    sprintf(logBuff,"Received Mission Request List from %d: Target: %d Component: %d", msg.sysid,mavlink_msg_mission_request_list_get_target_system(&msg), mavlink_msg_mission_request_list_get_target_component(&msg));
	    ParrDLOG(logBuff);

	    if (mavlink_msg_mission_request_list_get_target_component(&msg) == MAV_COMP_ID_MISSIONPLANNER)
	    {
	      //Reply with Mission Count
	      mavlink_msg_mission_count_pack(AC_ID, MAV_COMP_ID_MISSIONPLANNER ,&msg, targetSystem, MAV_COMP_ID_MISSIONPLANNER ,nb_waypoint);
	      sendMavlinkMsgToGS(&msg);
	    }
	    else
	    {
	      sprintf(logBuff,"Unknown target component for mission request list: %d", mavlink_msg_mission_request_list_get_target_component(&msg));
	      ParrDLOG(logBuff);
	    }
	  }
	  break;
	case MAVLINK_MSG_ID_MISSION_REQUEST:
	  {
		int targetSystem 	= msg.sysid; //mavlink_msg_mission_request_list_get_target_system(&msg);
		int targetComponent = msg.compid; //mavlink_msg_mission_request_list_get_target_component(&msg);
		int i = mavlink_msg_mission_request_get_seq(&msg);
		
		
		uint8_t wpCmd = MAV_CMD_NAV_WAYPOINT; // Define the waypoint action here!
		uint8_t current = 0; 		// Mission item is the current item
		
		if (i == 0) {current = 1;}	// Make the first waypoint the active one! REPLACE THIS WITH A CHECK!
		
		uint8_t autocontinue = 1; 	// Autocontinue to next WP
		float radius = 0;		// ONLY FOR NAV: Radius of acceptance
		float stayTime = 0;		// ONLY FOR NAV: Time to stay within the radius [miliseconds]
		float orbitRadius = 2;		// ONLY FOR LOITER: Radius of loitering
		float yawAngle = 0;		// FOR NAV OR LOITER: heading direction (0=NORTH)
		
		mavlink_msg_mission_item_pack(AC_ID, targetComponent, &msg,
						targetSystem, targetComponent, i, MAV_FRAME_LOCAL_ENU, wpCmd,current, autocontinue, radius,
						stayTime, orbitRadius, yawAngle, 
						RAD_OF_EM7RAD(waypoints[i].x),	// X Coordinate 
						RAD_OF_EM7RAD(waypoints[i].y),	// Y Coordinate 
						RAD_OF_EM7RAD(waypoints[i].z));	// Z Coordinate
		sendMavlinkMsgToGS(&msg);
	  }
	  break;
	case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
	  //Requested parameter list for a component
	  sprintf(logBuff,"Received Parameter Request List from: %d: Target: %d Component: %d", msg.sysid,mavlink_msg_param_request_list_get_target_system(&msg), mavlink_msg_param_request_list_get_target_component(&msg));
	  ParrDLOG(logBuff);
	  
	  mavlink_msg_param_value_pack(AC_ID, MAV_COMP_ID_IMU, &msg,"test_param_1", 1.73, MAVLINK_TYPE_DOUBLE, 1, 0);
	  mavlink_msg_param_value_pack(AC_ID, MAV_COMP_ID_IMU, &msg,"test_param_2", 2.15, MAVLINK_TYPE_DOUBLE, 1, 1);
	  sendMavlinkMsgToGS(&msg);
	  
	  break;
	case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
	  mavlink_msg_param_request_read_get_param_id(&msg, &tmpBuff);
	  sprintf(logBuff,"Received Parameter Read Request from %d.Target: %d Component: %d ID: %s Index: %d - NOT HANDLED!!!",msg.sysid, mavlink_msg_param_request_read_get_target_system(&msg), mavlink_msg_param_request_read_get_target_component(&msg), tmpBuff, mavlink_msg_param_request_read_get_param_index(&msg));
	  ParrDLOG(logBuff);
	  
	  break;
	case MAVLINK_MSG_ID_COMMAND_LONG:
	{
	  mavlink_command_long_t cmd;
	  mavlink_msg_command_long_decode(&msg,&cmd);
	  
	  sprintf(logBuff,"RECEIVED: Command: from %d.Target: %d Component: %d ", msg.sysid, cmd.target_system, cmd.target_component);
	  ParrDLOG(logBuff);
	  
	  switch (cmd.command)
	  {
	    case MAV_CMD_NAV_TAKEOFF:
	      sprintf(logBuff,"\tTAKE-OFF. Pitch: %.2f Yaw: %.2f Lat: %.2f Long: %.2f Alt: %.2f",cmd.param1, cmd.param4, cmd.param5, cmd.param6, cmd.param7);
	      ParrDLOG(logBuff);
	      break;
	    default:
	      sprintf(logBuff,"\tUNKNOWN! ID:%d", cmd.command);
	      ParrDLOG(logBuff);
	  }
	}
	  break;
	case MAVLINK_MSG_ID_MISSION_ACK:
	  break;
	case MAVLINK_MSG_ID_RC_CHANNELS_OVERRIDE:
	{
	    mavlink_rc_channels_override_t cmd;
	    mavlink_msg_rc_channels_override_decode(&msg,&cmd);
    
	    sprintf(logBuff,"RECEIVED: RC Channel Override for: %d/%d: CH1: %d; CH1: %d; CH1: %d; CH1: %d; CH1: %d; CH1: %d; CH1: %d; CH1: %d; ",
	      cmd.target_system, cmd.target_component, cmd.chan1_raw, cmd.chan2_raw, cmd.chan3_raw, cmd.chan4_raw, cmd.chan5_raw, cmd.chan6_raw, cmd.chan7_raw, cmd.chan8_raw);
	    ParrDLOG(logBuff);
	    
	    mavlink_msg_rc_channels_raw_pack(cmd.target_system,cmd.target_component, &msg, microsSinceEpoch(), 
				      1, 
				      cmd.chan1_raw, 
				      cmd.chan2_raw,
				      cmd.chan3_raw, 
				      cmd.chan4_raw, 
				      65535, 
				      65535, 
				      65535, 
				      65535, 
				      255); // Nondetermined signal strength
	    sendMavlinkMsgToGS(&msg);
	}
	break;
	case MAVLINK_MSG_ID_REQUEST_DATA_STREAM:
	{
	    mavlink_request_data_stream_t cmd;
	    mavlink_msg_request_data_stream_decode(&msg,&cmd);
/*
 uint16_t req_message_rate; ///< The requested interval between two messages of this type
 uint8_t target_system; ///< The target requested to send the message stream.
 uint8_t target_component; ///< The target requested to send the message stream.
 uint8_t req_stream_id; ///< The ID of the requested data stream
 uint8_t start_stop; ///< 1 to start sending, 0 to stop sending.
*/	    
	    sprintf(logBuff,"RECEIVED: Datastream Request: %d/%d, Stream ID: %d, Start/Stop: %d Rate: %d",
	      cmd.target_system, cmd.target_component, cmd.req_stream_id, cmd.start_stop, cmd.req_message_rate);
	    ParrDLOG(logBuff);
	    
	    mavlink_msg_data_stream_pack(AC_ID, cmd.target_component, &msg,
						       cmd.req_stream_id, cmd.req_message_rate,0);
	    sendMavlinkMsgToGS(&msg);
	}
	  break;
	case MAVLINK_MSG_ID_SET_MODE:
	{
	  mavlink_set_mode_t cmd;
	  mavlink_msg_set_mode_decode(&msg,&cmd);
	  
	 
	  sprintf(logBuff,"RECEIVED: SetMode Request: Target: %d BaseMode: %d CustomMode: %d", cmd.target_system, cmd.base_mode, cmd.custom_mode);
	  ParrDLOG(logBuff);
	}
	  break;
	default:
	  // Packet received
	  sprintf(logBuff,"UNKNOWN: Received packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
	  ParrDLOG(logBuff);
	}
      }
    }
    printf("\n");
  }
}

/* QNX timer version */
#if (defined __QNX__) | (defined __QNXNTO__)
uint64_t microsSinceEpoch()
{
	
	struct timespec time;
	
	uint64_t micros = 0;
	
	clock_gettime(CLOCK_REALTIME, &time);  
	micros = (uint64_t)time.tv_sec * 100000 + time.tv_nsec/1000;
	
	return micros;
}
#else
uint64_t microsSinceEpoch()
{
	
	struct timeval tv;
	
	uint64_t micros = 0;
	
	gettimeofday(&tv, NULL);  
	micros =  ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;
	
	return micros;
}
#endif


int sendMavlinkMsgToGS(const mavlink_message_t *msg)
{
  uint16_t len;
  uint8_t buf[MAVSEND_BUFFER_LENGTH];
  
  len = mavlink_msg_to_send_buffer(buf, msg);
  return sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
}

#ifdef SIM_DEBUG  
void ParrDLOG_clear()
{
  if( remove(DEBUG_FILE_LOCATION) != 0 )
  {
    ParrDLOG("WARNING: Log could not be removed");
  }
}
void ParrDLOG(char* msg)
{
  // Get time
  time_t rawtime;
  time ( &rawtime );
  
  //Print stuff to file
  FILE *file;
  file = fopen(DEBUG_FILE_LOCATION,"a+"); 
  fprintf(file,"%d: %s%s",(int)rawtime , msg,"\n");
  fclose(file); 
#endif
}